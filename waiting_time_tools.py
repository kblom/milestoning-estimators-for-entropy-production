#Analytical packages
import numpy as np

#Numba package
import numba as nb
from numba import njit, b1, i1, i8, f8

#--------------------------------------
#--------------------------------------

def waiting_time_module(traj):
    
    #Step 1: Calculate all waiting times
    waiting_times = np.diff(np.where(np.diff(traj)!=0)[0], prepend = -1)
    
    #Step 2: Remove duplicates in the trajectory
    traj_short = traj[np.insert(np.diff(traj).astype(np.bool), 0, True)]
    
    #Step 3: Collect statistics for each of the milestones
    psi0min, psi0plus, psi1plus, psi1min, psi2min, psi2plus = waiting_time_analyzer(waiting_times, traj_short)
    
    return(psi0min, psi0plus, psi1plus, psi1min, psi2min, psi2plus)

#--------------------------------------

@njit('Tuple((i8[:],i8[:],i8[:],i8[:],i8[:],i8[:]))(i8[:],i8[:])')
def waiting_time_analyzer(waiting_times, traj_short):
    
    #Set temporary
    temp = len(waiting_times)
    
    #Initialize counters
    count1, count2, count3, count4, count5, count6 = 0,0,0,0,0,0
    
    #Initialize data arrays for each milestone (assuming 3 milestones)
    psiaplus, psiamin = np.zeros((temp), dtype = i8), np.zeros((temp), dtype = i8)
    psibplus, psibmin = np.zeros((temp), dtype = i8), np.zeros((temp), dtype = i8)
    psicplus, psicmin = np.zeros((temp), dtype = i8), np.zeros((temp), dtype = i8)
    
    #Collect waiting-time statistics (assuming 3 milestones)
    for i in range(1,temp):
        if traj_short[i] == 0:
            if traj_short[i-1] == 1 and traj_short[i+1] == 2: 
                psiamin[count1] = waiting_times[i]
                count1 += 1
            if traj_short[i-1] == 2 and traj_short[i+1] == 1: 
                psiaplus[count2] = waiting_times[i]
                count2 += 1
        elif traj_short[i] == 1:
            if traj_short[i-1] == 0 and traj_short[i+1] == 2: 
                psibplus[count3] = waiting_times[i]
                count3 += 1
            if traj_short[i-1] == 2 and traj_short[i+1] == 0: 
                psibmin[count4] = waiting_times[i]
                count4 += 1
        else:
            if traj_short[i-1] == 0 and traj_short[i+1] == 1: 
                psicmin[count5]  = waiting_times[i]
                count5 += 1
            if traj_short[i-1] == 1 and traj_short[i+1] == 0: 
                psicplus[count6] = waiting_times[i]
                count6 += 1
                
    return(psiamin[0:count1], psiaplus[0:count2], 
           psibplus[0:count3], psibmin[0:count4], 
           psicmin[0:count5], psicplus[0:count6])

#--------------------------------------
#--------------------------------------

def analytical_waiting_times(Nsites,Nmilestones,pbias,k_arr):

    #Initialize data arrays
    psi0plusplusana, psi0minminana, psi0minplusana, psi0plusminana = np.zeros(len(k_arr)), np.zeros(len(k_arr)),  np.zeros(len(k_arr)), np.zeros(len(k_arr))
    psi1plusplusana, psi1minminana, psi1minplusana, psi1plusminana = np.zeros(len(k_arr)), np.zeros(len(k_arr)),  np.zeros(len(k_arr)), np.zeros(len(k_arr))
    psi2plusplusana, psi2minminana, psi2minplusana, psi2plusminana = np.zeros(len(k_arr)), np.zeros(len(k_arr)),  np.zeros(len(k_arr)), np.zeros(len(k_arr))
    
    #Construct milestonelist
    milestonelist = np.ceil(Nsites*np.arange(0,Nmilestones,1)/Nmilestones)
    
    #Construct milestone distances
    dist_arr = np.mod(np.roll(milestonelist, -1)-milestonelist,Nsites)-1

    #Set milestone territories
    Delta0min, Delta0plus = dist_arr[-1]*(Nsites-1)+1, (dist_arr[0]+1)*(Nsites-1)
    Delta1min, Delta1plus = dist_arr[0]*(Nsites-1)+1, (dist_arr[1]+1)*(Nsites-1)
    Delta2min, Delta2plus = dist_arr[1]*(Nsites-1)+1, (dist_arr[2]+1)*(Nsites-1)
    Delta0 = np.int(Delta0min + Delta0plus)
    Delta1 = np.int(Delta1min + Delta1plus)
    Delta2 = np.int(Delta2min + Delta2plus)

    #Construct analytical solutions (see Eq. (20) in manuscript)
    for i in range(1, Delta0): 
        temp = np.cos(i*np.pi/Delta0)**(k_arr-1)*np.sin(i*np.pi/Delta0)
        psi0plusplusana += temp*np.sin(i*np.pi*Delta0plus/Delta0)
        psi0minminana   += temp*np.sin(i*np.pi*Delta2plus/Delta0)
        psi0minplusana  += temp*np.sin(i*np.pi*Delta0min/Delta0)
        psi0plusminana  += temp*np.sin(i*np.pi*Delta1min/Delta0)
    for i in range(1, Delta1): 
        temp = np.cos(i*np.pi/Delta1)**(k_arr-1)*np.sin(i*np.pi/Delta1)
        psi1plusplusana += temp*np.sin(i*np.pi*Delta1plus/Delta1)
        psi1minminana   += temp*np.sin(i*np.pi*Delta0plus/Delta1)
        psi1minplusana  += temp*np.sin(i*np.pi*Delta1min/Delta1)
        psi1plusminana  += temp*np.sin(i*np.pi*Delta2min/Delta1)
    for i in range(1, Delta2): 
        temp = np.cos(i*np.pi/Delta2)**(k_arr-1)*np.sin(i*np.pi/Delta2)
        psi2plusplusana += temp*np.sin(i*np.pi*Delta2plus/Delta2)
        psi2minminana   += temp*np.sin(i*np.pi*Delta1plus/Delta2)
        psi2minplusana  += temp*np.sin(i*np.pi*Delta2min/Delta2)
        psi2plusminana  += temp*np.sin(i*np.pi*Delta0min/Delta2)
    
    #Prefactors
    temp1  = (4*(1-pbias)*pbias)**(k_arr/2)
    temp2 = 1/pbias-1
    psi0plusplusana  *= temp1*temp2**(Delta0plus/2)
    psi0plusminana   *= temp1*temp2**(Delta1min/2)
    psi0minminana    *= temp1*temp2**(-Delta2plus/2)
    psi0minplusana   *= temp1*temp2**(-Delta0min/2)
    
    psi1plusplusana  *= temp1*temp2**(Delta1plus/2)
    psi1plusminana   *= temp1*temp2**(Delta2min/2)
    psi1minminana    *= temp1*temp2**(-Delta0plus/2)
    psi1minplusana   *= temp1*temp2**(-Delta1min/2)
    
    psi2plusplusana *= temp1*temp2**(Delta2plus/2)
    psi2plusminana   *= temp1*temp2**(Delta0min/2)
    psi2minminana    *= temp1*temp2**(-Delta1plus/2)
    psi2minplusana   *= temp1*temp2**(-Delta2min/2)
    
    #Normalize
    psi0plusplusana /= np.sum(psi0plusplusana)
    psi0plusminana  /= np.sum(psi0plusminana)
    psi0minminana   /= np.sum(psi0minminana) 
    psi0minplusana  /= np.sum(psi0minplusana)
    
    psi1plusplusana /= np.sum(psi1plusplusana)
    psi1plusminana  /= np.sum(psi1plusminana)
    psi1minminana   /= np.sum(psi1minminana) 
    psi1minplusana  /= np.sum(psi1minplusana) 
    
    psi2plusplusana /= np.sum(psi2plusplusana)
    psi2plusminana  /= np.sum(psi2plusminana)
    psi2minminana   /= np.sum(psi2minminana) 
    psi2minplusana  /= np.sum(psi2minplusana) 

    
    return(psi0plusplusana, psi0minminana, psi0minplusana,  psi0plusminana, 
           psi1plusplusana, psi1minminana, psi1minplusana,  psi1plusminana, 
           psi2plusplusana, psi2minminana, psi2minplusana,  psi2plusminana)

#--------------------------------------
#--------------------------------------

def analytical_steady_state_probabilities(Nsites,Nmilestones,pbias):
    
    
    #Construct milestonelist
    milestonelist = np.ceil(Nsites*np.arange(0,Nmilestones,1)/Nmilestones)
    
    #Construct milestone distances
    dist_arr = np.mod(np.roll(milestonelist, -1)-milestonelist,Nsites)-1

    #Set milestone territories
    Delta0min, Delta0plus = dist_arr[-1]*(Nsites-1)+1, (dist_arr[0]+1)*(Nsites-1)
    Delta1min, Delta1plus = dist_arr[0]*(Nsites-1)+1, (dist_arr[1]+1)*(Nsites-1)
    Delta2min, Delta2plus = dist_arr[1]*(Nsites-1)+1, (dist_arr[2]+1)*(Nsites-1)
    
    #Construct steady-state probabilities
    alpha = pbias/(1-pbias)
    pi0   = (1-alpha**(Delta2min))*(1-alpha**(Delta0min+Delta1min))
    pi1   = (1-alpha**(Delta0min))*(1-alpha**(Delta1min+Delta2min))
    pi2   = (1-alpha**(Delta1min))*(1-alpha**(Delta2min+Delta0min))
    pisum = pi0+pi1+pi2
    
    #Normalize
    pi0 /= pisum
    pi1 /= pisum
    pi2 /= pisum
    
    #Construct jump probabilities
    p0plus = (1-alpha**(Delta0min))/(1-alpha**(Delta0min+Delta1min))
    p1plus = (1-alpha**(Delta1min))/(1-alpha**(Delta1min+Delta2min))
    p2plus = (1-alpha**(Delta2min))/(1-alpha**(Delta2min+Delta0min))
        
    #Construct waiting time
    tau = (Nsites-1)*(Delta0plus+Delta1plus+Delta2plus)/((1-2*pbias)*(3-2*(1/(1-alpha**(-Delta0min))+1/(1-alpha**(-Delta1min))+1/(1-alpha**(-Delta2min)))))
                                                                       
    #Exact entropy
    Sexact = (2*pbias-1)*np.log(alpha)/(Nsites-1)
    
    #First order entropy
    S1exact = ((pi0*p0plus-pi1*(1-p1plus))*np.log(pi0*p0plus/(pi1*(1-p1plus)))
               +(pi1*p1plus-pi2*(1-p2plus))*np.log(pi1*p1plus/(pi2*(1-p2plus)))
               +(pi2*p2plus-pi0*(1-p0plus))*np.log(pi2*p2plus/(pi0*(1-p0plus))))/tau
    S1ana = (3/(Nsites-1)+Nsites-3)*Sexact/Nsites
    
    #Construct splitting probabilities
    phi0plusplus, phi0minmin = (1-alpha**(Delta0min))/(1-alpha**(Delta0min+Delta0plus)),1-(1-alpha**(Delta2plus))/(1-alpha**(Delta0min+Delta0plus))
    phi1plusplus, phi1minmin = (1-alpha**(Delta1min))/(1-alpha**(Delta1min+Delta1plus)),1-(1-alpha**(Delta0plus))/(1-alpha**(Delta1min+Delta1plus))
    phi2plusplus, phi2minmin = (1-alpha**(Delta2min))/(1-alpha**(Delta2min+Delta2plus)),1-(1-alpha**(Delta1plus))/(1-alpha**(Delta2min+Delta2plus))
    
    #Construct second-order jump probabilities
    p0plusplus, p0minmin = p2plus*phi0plusplus, (1-p1plus)*phi0minmin
    p1plusplus, p1minmin = p0plus*phi1plusplus, (1-p2plus)*phi1minmin
    p2plusplus, p2minmin = p1plus*phi2plusplus, (1-p0plus)*phi2minmin
    
    #Second order entropy
    S2exact = ((pi2*p0plusplus-pi1*p0minmin)*np.log(phi0plusplus/phi0minmin)
               +(pi0*p1plusplus-pi2*p1minmin)*np.log(phi1plusplus/phi1minmin)
               +(pi1*p2plusplus-pi0*p2minmin)*np.log(phi2plusplus/phi2minmin))/tau
    
    return(pi0,pi1,pi2,p0plus,p1plus,p2plus,Sexact,S1exact,S1ana,S2exact)