#Analytical packages
import numpy as np

#Numba package
import numba as nb
from numba import njit, b1, i1, i8, f8

@njit('Tuple((i8[:],i8[:],i8[:]))(i8,i8,i8,i8,f8,f8,b1)')
def ringgenerator(Nmacro,Nmicro,Nsteps,Nmilestones,pbiasmacro,pbiasmicro,modelB):
    
    #Set temporaries
    temp  = np.floor(Nmicro/2)
    temp2 = np.floor(Nmicro/4)
    temp3 = np.floor(3*Nmicro/4)
    
    #Construct milestonelist
    milestonelist = np.floor(Nmacro*np.arange(0,Nmilestones,1)/Nmilestones)
  
    #Initialize data arrays: full + lumped + post-lumped milestoned trajectory
    full_config   = np.zeros((Nsteps), dtype = i8)
    lumped_config = np.zeros((Nsteps), dtype = i8)
    mil_config    = np.zeros((Nsteps), dtype = i8)
    
    #Generate random initial configuration
    configmacro = np.random.randint(Nmacro)
    configmicro = np.random.randint(Nmicro)
    
    #Store initial configuration
    full_config[0]   = Nmicro*configmacro+configmicro
    lumped_config[0] = configmacro
    mil_config[0]    = -1
    
    #Run trajectory
    for i in range(1,Nsteps):
        
        #Make a jump
        if configmicro == 0:
            if np.random.random() <= pbiasmacro:
                configmicro = temp
                configmacro = (configmacro-1)%Nmacro
            else:
                direction  = 1-2*(np.random.random()<=pbiasmicro)
                configmicro = (configmicro+direction)%Nmicro
        elif configmicro == temp:
            if np.random.random() <= 1-pbiasmacro:
                configmicro = 0
                configmacro = (configmacro+1)%Nmacro
            else:
                direction  = 1-2*(np.random.random()<=pbiasmicro)
                configmicro = (configmicro+direction)%Nmicro
        elif modelB == True and configmicro == temp2:
            if np.random.random() <= 1-pbiasmacro:
                configmicro = temp3
                configmacro = (configmacro+1)%Nmacro
            else:
                direction  = 1-2*(np.random.random()<=pbiasmicro)
                configmicro = (configmicro+direction)%Nmicro
        elif modelB == True and configmicro == temp3:
            if np.random.random() <= pbiasmacro:
                configmicro = temp2
                configmacro = (configmacro-1)%Nmacro
            else:
                direction  = 1-2*(np.random.random()<=pbiasmicro)
                configmicro = (configmicro+direction)%Nmicro
        else:
            direction  = 1-2*(np.random.random()<=pbiasmicro)
            configmicro = (configmicro+direction)%Nmicro    

        #Store new configuration
        full_config[i]   = Nmicro*configmacro+configmicro
        lumped_config[i] = configmacro
        if configmacro in milestonelist: mil_config[i] = np.where(milestonelist == configmacro)[0][0]
        else: mil_config[i] = mil_config[i-1]
            
    #Set initial milestone
    lastind = np.where(mil_config == -1)[0][-1]
    mil_config[0:lastind+1] = mil_config[lastind+1]
    
    return(full_config, lumped_config, mil_config)
            