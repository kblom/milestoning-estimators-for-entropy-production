This source code has been used in the following paper: _Milestoning estimators of dissipation in systems observed at a coarse resolution_ 

Authors: Kristian Blom, Kevin Song, Etienne Vouga, Aljaz Godec, Dmitrii Makarov

For the results shown in Figure 1 the following publicly available Mathematica notebook can be used: https://www.wolframcloud.com/obj/kblom/Published/Entropy_production_periodic_potential.nb

For the other figures the following version of Python was used: 3.9.12

The following Python packages are required to run the source code: matplotlib, numpy, math, and numba.

Summary of the main files:

- Ring-Notebook.ipynb: Jupyter notebook to calculate the entropy production for the discrete-time Markov chain shown in Figure 2. The notebook also contains the raw data for the results shown in Figure 2.

- Single-File-Notebook.ipynb: Jupyter notebook to calculate the entropy production + waiting-time distributions for the discrete-time single-file system shown in Figure 3. The notebook also contains the raw data for the results shown in Figures 3, 5, and S3.

- ring_simulations_tools.py: Source code for stochastic trajectories of the discrete-time Markov chain shown in Figure 2. For an example of stochastic trajectories see Figure S2a-c.

- single_file_simulations_tools.py: Source code for stochastic trajectories of the discrete-time single-file system shown in Figure 3. For an example of stochastic trajectories see Figure S2d-f. 

- entropy_tools.py: Source code to determine the entropy production from a given stochastic trajectory. 

- waiting_time_tools.py: Source code to determine the waiting-time distribution for 3 milestones from a given stochastic trajectory. This code also contains the analytical expressions for the single-file waiting-time distribution.  

- Raw_data_waiting_times: Contains the raw data (.npy files) of the waiting-time distributions shown in Figures 5.

Please make sure all above files are in the same folder before executing the Jupyter notebooks. 
