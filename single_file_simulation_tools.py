#Analytical packages
import numpy as np
import math

#Numba package
import numba as nb
from numba import njit, b1, i1, i8, f8

#Single-file with uniform bias
#--------------------------------------
@njit('Tuple((i8[:],i8[:],i8[:]))(i8,i1,i1,i1,f8,i1,b1)')
def single_file(Nsteps,Nsites,Nvacancies,Nmilestones,pbias,scenario,storewaitingtime):

    #Define tagged particle
    tagged = Nsites-Nvacancies
    
    #Define auxiliary functions
    base     = tagged+1
    slot_arr = np.arange(1,Nsites+1,1)
    
    #Number of microscopic states
    Nstates = tagged*int(math.gamma(Nsites+1)/(math.gamma(Nvacancies+1)*math.gamma(Nsites-Nvacancies+1)))
    
    #Construct milestonelist
    milestonelist = np.ceil(Nsites*np.arange(0,Nmilestones,1)/Nmilestones)
    
    #Construct milestone dictionary
    milestonedict = {milestonelist[0] : 0}
    for ind, i in enumerate(milestonelist[1::]): milestonedict[i] = ind+1
    
    #Initialize counter for dictionary
    basearray = base**(Nsites-slot_arr)
    statelist = np.arange(1,Nstates+1,1)
    count = 1
    
    #Generate random initial condition
    config = np.arange(1,Nsites+1,1)-Nvacancies
    config[0:Nvacancies] = 0
    np.random.shuffle(config)
    
    #Convert configuration to unique number
    tempb = np.sum(config*base**(slot_arr[::-1]-1))
    
    #Set initial dictionary
    configdict = {tempb : 0}
    
    #Initialize data arrays: full + lumped + post-lumped milestoned trajectory
    full_config   = np.zeros((Nsteps), dtype = i8)
    lumped_config = np.zeros((Nsteps), dtype = i8)
    mil_config    = np.zeros((Nsteps), dtype = i8)
    
    #Store initial configuration
    full_config[0]   = 0
    lumped_config[0] = np.where(config == tagged)[0][0]
    mil_config[0]    = -1
    
    #Store particle positions
    particlelist = np.where(config != 0)[0]
    
    #Run trajectory
    i = 1
    while i < Nsteps:
        
        #Pick a random particle
        ind = np.random.randint(tagged)
        particleposition = particlelist[ind]
        
        #Decide for a random direction
        #Here we have different scenarios: 0 - uniform bias, 1 - tagged particle bias, 2 - bath particle bias
        if scenario == 0: direction = 2*(np.random.random()<=pbias)-1
        elif scenario == 1:
            if particleposition == lumped_config[i-1]: direction = 2*(np.random.random()<=pbias)-1
            else:                                      direction = 2*(np.random.random()<=1/2)-1
        elif scenario == 2:
            if particleposition == lumped_config[i-1]: direction = 2*(np.random.random()<=1/2)-1
            else:                                      direction = 2*(np.random.random()<=pbias)-1
        
        #Index of new particle position
        newparticleposition = (particleposition+direction)%Nsites
        
        #Don't perform move when new position is occupied
        if newparticleposition in particlelist:
            if storewaitingtime: 
                
                #Set new configurations
                full_config[i]   = full_config[i-1]
                lumped_config[i] = lumped_config[i-1]
                mil_config[i]    = mil_config[i-1]
                
                #Update counter
                i += 1
                
        #Perform move when new position is unoccupied
        else:
            config[newparticleposition] = config[particleposition]
            config[particleposition]    = 0
            
            #Store lumped + post-lumped milestoned trajectory
            if particleposition == lumped_config[i-1]:
                lumped_config[i] = newparticleposition
                if newparticleposition in milestonelist:
                    mil_config[i] = milestonedict[newparticleposition]
                else:
                    mil_config[i] = mil_config[i-1]
            else:
                lumped_config[i] = lumped_config[i-1]
                mil_config[i]    = mil_config[i-1]
                
            #Update particle list
            particlelist[ind] = newparticleposition
            
            #Translate full configuration into a unique number
            tempb += config[newparticleposition]*(basearray[newparticleposition]-basearray[particleposition])
            
            #Store full trajectory
            try: full_config[i] = configdict[tempb]
            except:
                configdict[tempb] = count
                full_config[i] = count
                count += 1
                
            #Update counter
            i += 1
            
    #Set initial value of milestone
    lastind = np.where(mil_config == -1)[0][-1]
    mil_config[0:lastind+1] = mil_config[lastind+1]
    
    return(full_config, lumped_config, mil_config)


#Backward milestoned trajectory
#--------------------------------------
@njit(i8[:](i8,i1,i1,i8[:]))
def backward_milestoned_trajectory(Nsteps,Nsites,Nmilestones,lump_traj):
    
    #Initialize array
    mil_traj_back, counter = np.zeros((Nsteps), dtype = i8), 0

    #Milestone list
    milestonelist = np.ceil(Nsites*np.arange(0,Nmilestones,1)/Nmilestones)

    #Determine backward milestoned trajectory
    for i in range(Nsteps): 
        if lump_traj[Nsteps-1-i] in milestonelist: 
            mil_traj_back[Nsteps-1-i] = np.where(lump_traj[Nsteps-1-i] == milestonelist)[0][0]
            if counter == 0: counter = i
        elif i != 0: 
            mil_traj_back[Nsteps-1-i] = mil_traj_back[Nsteps-i]

    #Set initial milestone in the backward trajectory
    mil_traj_back[Nsteps-counter:Nsteps] = mil_traj_back[Nsteps-1-counter]
    
    return(mil_traj_back)