#Analytical packages
import numpy as np

#Numba package
import numba as nb
from numba import njit, b1, i1, i8, f8

#Half-way cut milestoned trajectory
#--------------------------------------
@njit(i8[:](i8[:], i8[:]))
def half_way_cut_mil(mil_traj, mil_traj_back):
    
    #Set difference to highlight transition region
    transit_region = mil_traj-mil_traj_back
    
    #Set counters
    counter1, counter2 = 0, -1

    for i in range(len(mil_traj)):
        if transit_region[i] != 0 and counter2 == -1:
            counter1 = i
            counter2 += 1
        elif transit_region[i] != 0:
            counter2 += 1
        elif counter2 != -1:
            transit_region[counter1:counter1+counter2//2+1] = 0
            counter2 = -1
                
    return(mil_traj-transit_region)

#Trajectory reshaper
#--------------------------------------
@njit(i8[:,:](i8[:], i8))
def traj_reshape(traj,korder):
    
    #Set length of trajectory
    Ntraj = traj.shape[0]
    
    #Initialize new array
    trajnew = np.zeros((Ntraj-korder,korder+1), dtype = i8)
    
    #Construct new array
    for i in range(Ntraj-korder): trajnew[i,:] = traj[i:i+1+korder]
    
    return(trajnew)

#Base inverter (crucial to obtain time reversed path)
#----------------------------------------
@njit(i8(i8, i8, i8))
def base_invert(index, length, base):
    
    #Initialize arrays
    base_repr = np.zeros(length, dtype = i8)
    base_arr  = base**(length-1-np.arange(0,length,1))

    #Create state array
    for j in range(length):
        
        #Create bit
        base_repr[length-1-j] = index%base
        
        #Update index
        index = index//base
        
    #Invert base array
    new_index = np.sum(base_repr[::-1]*base_arr)
        
    return(new_index)

#Entropy calculation for a discrete-time Markov process
#--------------------------------------
@njit(f8(i8[:],i8))
def markov_entropy_calculator(traj,korder):
    
    #Reshape the trajectory
    traj = traj_reshape(traj,korder)
    
    #Base numbers for data conversion
    base     = np.max(traj)+1
    base_arr = base**(korder-np.arange(0,korder+1,1))
            
    #Data conversion: For each possible snippet we create a unique number and store it as a trajectory
    convtraj = np.zeros(traj.shape[0], dtype = i8)
    for i in range(traj.shape[0]): convtraj[i] = np.sum(traj[i,:]*base_arr)

    #Count the total number of occurences for each snippet + normalize
    count = np.bincount(convtraj, minlength = base**(korder+1))
    
    #Calculate entropy production
    sk = 0
    for i in range(base**(korder+1)):
        temp1 = count[i]
        temp2 = count[base_invert(i,korder+1,base)]
        if temp1 != 0 and temp2 != 0: sk += (temp1-temp2)*np.log(temp1/temp2)

    #Normalization
    sk /= 2*korder*np.sum(count)

    return(sk)


#Entropy calculation for a discrete-time semi-Markov process
#--------------------------------------
#--------------------------------------
def semi_markov_entropy_module(traj, korder):
    
    #Calculate average time spend in a lump
    waiting_times = np.diff(np.where(np.diff(traj)!=0)[0], prepend = -1)
    lumptime      = np.mean(waiting_times)
    
    #Remove duplicates in the trajectory
    traj_short = traj[np.insert(np.diff(traj).astype(np.bool), 0, True)]
    
    #Calculate entropy in sequence of distinct states (two-step affinity)
    sA = semi_markov_affinity_entropy_calculator(traj_short, korder)
    
    return(sA/lumptime)

@njit(f8(i8[:],i8))
def semi_markov_affinity_entropy_calculator(traj,korder):
        
    #Reshape the trajectory
    traj = traj_reshape(traj,korder)
    
    #Base numbers for date conversion
    base     = np.max(traj)+1
    base_arr = base**(korder-np.arange(0,korder+1,1))
            
    #Data conversion: For each possible snippet we create a unique number and store it as a trajectory
    convtraj = np.zeros(traj.shape[0], dtype = i8)
    for i in range(traj.shape[0]): convtraj[i] = np.sum(traj[i,:]*base_arr)

    #Count the total number of occurences for each snippet + normalize
    count = np.bincount(convtraj, minlength = base**(korder+1))
    
    #Calculate entropy production
    sk = 0
    for i in range(base**(korder+1)):
        
        #Marginal probabilities
        invert_index = base_invert(i,korder+1,base)
        temp1 = count[i]
        temp2 = count[invert_index]
        
        #For splitting probabilities
        start_ind1 = i-i%base
        start_ind2 = invert_index-invert_index%base
        temp3 = np.sum(count[start_ind1:start_ind1+base])
        temp4 = np.sum(count[start_ind2:start_ind2+base])
        
        if temp1 != 0 and temp2 != 0: sk += (temp1-temp2)*np.log(temp1*temp4/(temp2*temp3))

    #Normalization
    sk /= 2*np.sum(count)

    return(sk)

#--------------------------------------
#--------------------------------------

#Entropy calculation based on the TUR
#--------------------------------------
@njit(f8(i8[:],i8,i8))
def TUR_entropy_calculator(traj,Nsites,Njump):
    
    #Track jumps between states
    steps = traj[Njump::]-traj[0:-Njump]
    steps[np.where(steps == Nsites-1)[0]] = -1
    steps[np.where(steps == 1-Nsites)[0]] = 1
    
    #Calculate mean and variance of current
    Jmean = np.mean(steps)
    Jvar  = np.var(steps)
    
    #Determine entropy production
    sk = 2*Jmean**2/(Jvar*Njump) 

    return(sk)